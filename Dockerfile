# Dockerfile for a minimal Golang application
# Reference: https://docs.docker.com/engine/reference/builder/

# Use upstream Golang image (https://hub.docker.com/_/golang) with the CERN registry pull-through cache:
# https://kubernetes.docs.cern.ch/docs/registry/quickstart/#pull-through-caches
FROM registry.cern.ch/docker.io/library/golang:1.18 as builder

WORKDIR /workspace

# Copy application source code into the build context
COPY /src ./

# Disable Cgo so we get a statically linked binary
ENV CGO_ENABLED=0

# Compile the binary
RUN go build -o server .

# Use distroless as minimal base image to package the manager binary
# Refer to https://github.com/GoogleContainerTools/distroless for more details
FROM gcr.io/distroless/static:nonroot
WORKDIR /
COPY --from=builder /workspace/server .

CMD ["./server"]
